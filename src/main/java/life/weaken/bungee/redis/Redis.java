package life.weaken.bungee.redis;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

import java.util.concurrent.TimeUnit;

public class Redis {

    private final JedisPool jedisPool;
    private final ProxyServer proxy;
    private final ScheduledTask updateTask;

    public Redis(Plugin plugin, RedisConfig config) {
        this.proxy = plugin.getProxy();

        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMinIdle(8);
        poolConfig.setMaxTotal(32);
        jedisPool = new JedisPool(poolConfig, config.getHost(), config.getPort(), config.getUsername(), config.getPassword());

        updateTask = proxy.getScheduler().schedule(plugin, this::updateOwnInformation, 1L, 1L, TimeUnit.SECONDS);

    }

    public void updateOwnInformation() {
        try(Jedis jedis = jedisPool.getResource()) {
            Pipeline pipeline = jedis.pipelined();
            pipeline.set("Bungee-LastUpdate", "" + System.currentTimeMillis());
            pipeline.set("Bungee-PlayerCount", "" + proxy.getPlayers().size());
            pipeline.set("Bungee-Servers", String.join(", ", proxy.getServers().keySet()));
            pipeline.sync();
        }
    }

    public void shutdown() {
        updateTask.cancel();
        updateOwnInformation();
        jedisPool.close();
    }

}
