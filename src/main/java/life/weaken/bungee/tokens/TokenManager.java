package life.weaken.bungee.tokens;

import life.weaken.bungee.tokens.storage.MariaDB;
import life.weaken.bungee.tokens.storage.MariaDBConfig;
import life.weaken.bungee.utils.FutureUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class TokenManager {

    private final ProxyServer server;
    private final MariaDB mariaDB;
    private final ScheduledTask tokenTask;

    private final Plugin plugin;

    public TokenManager(Plugin plugin, MariaDBConfig mariaDBConfig) {
        server = plugin.getProxy();
        this.plugin = plugin;
        mariaDB = new MariaDB(plugin, mariaDBConfig);
        tokenTask = server.getScheduler().schedule(plugin, () -> tokenAll(1), 10L, 10L, TimeUnit.MINUTES);
    }

    public void shutdown() {
        tokenTask.cancel();
        mariaDB.shutdown();
    }

    public void tokenAll(Integer amount) {
        for(ProxiedPlayer player : server.getPlayers()) giveToken(player, amount);
    }

    @SuppressWarnings("ConstantConditions")
    public void giveToken(ProxiedPlayer player, Integer amount) {
        CompletableFuture<Integer> tokens = mariaDB.getTokens(player);

        FutureUtils.waitForFuture(plugin, () -> {
            mariaDB.setTokens(player, FutureUtils.get(tokens) + amount);
            player.sendMessage(new ComponentBuilder("You recieved " + amount + " tokens").color(ChatColor.GREEN).create());
        }, tokens);
    }

    public CompletableFuture<Integer> getTokens(ProxiedPlayer player) {
        return mariaDB.getTokens(player);
    }

    public void setTokens(ProxiedPlayer player, Integer tokens) {
        mariaDB.setTokens(player, tokens);
    }

    public void unload(ProxiedPlayer player) {
        mariaDB.unload(player);
    }

}
