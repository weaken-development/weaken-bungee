package life.weaken.bungee.tokens.commands;

import life.weaken.bungee.tokens.TokenManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class TokenPayCommand extends Command implements TabExecutor {

    private final TokenManager manager;

    public TokenPayCommand(TokenManager manager) {
        super("tokenpay", "bungee.tokenpay");
        this.manager = manager;
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        return null;
    }
}