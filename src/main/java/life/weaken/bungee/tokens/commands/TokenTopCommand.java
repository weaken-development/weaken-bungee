package life.weaken.bungee.tokens.commands;

import life.weaken.bungee.tokens.TokenManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class TokenTopCommand extends Command implements TabExecutor {

    private final TokenManager manager;

    public TokenTopCommand(TokenManager manager) {
        super("tokentop", "bungee.tokentop");
        this.manager = manager;
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        return null;
    }
}
