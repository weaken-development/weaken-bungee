package life.weaken.bungee.tokens.commands;

import life.weaken.bungee.tokens.TokenManager;
import life.weaken.bungee.utils.FutureUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class TokensCommand extends Command implements TabExecutor {

    private final TokenManager manager;
    private final Plugin plugin;

    public TokensCommand(Plugin plugin, TokenManager manager) {
        super("tokens", "bungee.tokens");
        this.manager = manager;
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player;
        if(args.length < 1) {
            if(sender instanceof ProxiedPlayer) player = (ProxiedPlayer) sender;
            else {
                sender.sendMessage(new ComponentBuilder("Incorrect usage: /tokens <player>").color(ChatColor.RED).create());
                return;
            }
        } else {
            player = ProxyServer.getInstance().getPlayer(args[0]);
            if(player == null) {
                sender.sendMessage(new ComponentBuilder("That player is not currently online").color(ChatColor.RED).create());
                return;
            }
        }

        CompletableFuture<Integer> tokens = manager.getTokens(player);
        FutureUtils.waitForFuture(plugin, () ->
                sender.sendMessage(new ComponentBuilder(player.getName() + "'s tokens: " + FutureUtils.get(tokens)).color(ChatColor.GREEN).create()),
        tokens);

    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if(args.length == 1) {
            return ProxyServer.getInstance().getPlayers().stream()
                    .map(ProxiedPlayer::getName)
                    .filter(name -> name.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}