package life.weaken.bungee.tokens;

import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public record TokenListener(TokenManager manager) implements Listener {

    @EventHandler
    public void onDisconnect(PlayerDisconnectEvent event) {
        manager.unload(event.getPlayer());
    }

}
