package life.weaken.bungee.tokens.storage;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import org.mariadb.jdbc.MariaDbPoolDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class MariaDB {

    private final MariaDbPoolDataSource connectionPool;
    private final HashMap<ProxiedPlayer, Integer> tokenMap;
    private final ScheduledTask saveTask;

    private final TaskScheduler scheduler;
    private final Plugin plugin;

    public MariaDB(Plugin plugin, MariaDBConfig config) {
        this.plugin = plugin;
        scheduler = plugin.getProxy().getScheduler();

        connectionPool = new MariaDbPoolDataSource();
        tokenMap = new HashMap<>();

        try {
            connectionPool.setServerName(config.getHost());
            connectionPool.setDatabaseName(config.getDatabase());
            connectionPool.setPort(config.getPort());
            connectionPool.setUser(config.getUsername());
            connectionPool.setPassword(config.getPassword());
            connectionPool.setMinPoolSize(8);
            connectionPool.setMaxPoolSize(32);
        } catch(SQLException e) {
            e.printStackTrace();
        }

        try(Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(true);
            try(Statement statement = connection.createStatement()) {
                statement.execute("CREATE TABLE IF NOT EXISTS tokens(id CHAR(32) PRIMARY KEY, value INTEGER NOT NULL);");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }

        saveTask = plugin.getProxy().getScheduler().schedule(plugin, () -> scheduler.runAsync(plugin, this::saveAll), 0L, 1L, TimeUnit.MINUTES);
    }

    private String id(ProxiedPlayer player) {
        return player.getUniqueId().toString().replace("-", "");
    }

    @SuppressWarnings("unchecked")
    private void saveAll() {
        HashMap<ProxiedPlayer, Integer> clone = (HashMap<ProxiedPlayer, Integer>) tokenMap.clone();
        try(Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(false);
            for(ProxiedPlayer player : clone.keySet()) {
                try(Statement statement = connection.createStatement()) {
                    statement.execute("REPLACE INTO tokens(id, value) VALUES ('" + id(player) + "', " + clone.get(player) + ");");
                }
            }
            connection.commit();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    private void save(ProxiedPlayer player) {
        Integer tokens = tokenMap.get(player);
        if(tokens == null) return;
        scheduler.runAsync(plugin, () -> {
            try(Connection connection = connectionPool.getConnection()) {
                connection.setAutoCommit(true);
                try(Statement statement = connection.createStatement()) {
                    statement.execute("REPLACE INTO tokens(id, value) VALUES ('" + id(player) + "', " + tokens + ");");
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private CompletableFuture<Integer> load(ProxiedPlayer player) {
        CompletableFuture<Integer> future = new CompletableFuture<>();
        scheduler.runAsync(plugin, () -> {
            try (Connection connection = connectionPool.getConnection()) {
                try (Statement statement = connection.createStatement()) {
                    ResultSet resultSet = statement.executeQuery("SELECT * FROM tokens WHERE id='" + id(player) + "';");
                    int tokens = 0;
                    if (resultSet.next()) tokens = resultSet.getInt("value");
                    tokenMap.put(player, tokens);
                    future.complete(tokens);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        return future;
    }

    public CompletableFuture<Integer> getTokens(ProxiedPlayer player) {
        if(!tokenMap.containsKey(player)) return load(player);
        CompletableFuture<Integer> future = new CompletableFuture<>();
        future.complete(tokenMap.get(player));
        return future;
    }

    public void setTokens(ProxiedPlayer player, Integer value) {
        tokenMap.put(player, value);
    }

    public void unload(ProxiedPlayer player) {
        save(player);
        tokenMap.remove(player);
    }

    public void shutdown() {
        saveTask.cancel();
        saveAll();
        connectionPool.close();
    }

}
