package life.weaken.bungee;

import life.weaken.bungee.commands.AnnounceCommand;
import life.weaken.bungee.commands.LobbyCommand;
import life.weaken.bungee.tokens.storage.MariaDBConfig;
import life.weaken.bungee.redis.Redis;
import life.weaken.bungee.redis.RedisConfig;
import life.weaken.bungee.tokens.TokenListener;
import life.weaken.bungee.tokens.TokenManager;
import life.weaken.bungee.tokens.commands.TokenPayCommand;
import life.weaken.bungee.tokens.commands.TokenTopCommand;
import life.weaken.bungee.tokens.commands.TokensCommand;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class BungeePlugin extends Plugin {

    private Redis redis;
    private TokenManager tokenManager;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void onEnable() {
        if(!getDataFolder().exists()) getDataFolder().mkdirs();
        saveDefaultConfig();

        Configuration config = null;
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(config == null) return;

        redis = new Redis(this, new RedisConfig(
                config.getString("redis.host"),
                config.getInt("redis.port"),
                config.getString("redis.username"),
                config.getString("redis.password")
        ));

        tokenManager = new TokenManager(this, new MariaDBConfig(
                config.getString("mariadb.host"),
                config.getInt("mariadb.port"),
                config.getString("mariadb.username"),
                config.getString("mariadb.password"),
                config.getString("mariadb.database")
        ));
        getProxy().getPluginManager().registerListener(this, new TokenListener(tokenManager));

        registerCommands(config);
    }

    public void registerCommands(Configuration config) {
        PluginManager pluginManager = getProxy().getPluginManager();
        pluginManager.registerCommand(this, new LobbyCommand(config.getString("lobby-server")));
        pluginManager.registerCommand(this, new AnnounceCommand());
        pluginManager.registerCommand(this, new TokensCommand(this, tokenManager));
        //pluginManager.registerCommand(this, new TokenTopCommand(tokenManager));
        //pluginManager.registerCommand(this, new TokenPayCommand(tokenManager));
    }

    @Override
    public void onDisable() {
        redis.shutdown();
        tokenManager.shutdown();
    }

    private void saveDefaultConfig() {
        File configFile = new File(getDataFolder(), "config.yml");
        if(!configFile.exists()) {
            try(InputStream in = getResourceAsStream("config.yml")) {
                Files.copy(in, configFile.toPath());
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

}
