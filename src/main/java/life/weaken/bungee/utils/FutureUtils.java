package life.weaken.bungee.utils;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.Plugin;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class FutureUtils {

    public static void waitForFuture(Plugin plugin, Runnable runnable, Future<?> future) {
        new BungeeRunnable() {
            @Override
            public void run() {
                if(future.isCancelled()) this.cancel();
                if(future.isDone()) {
                    runnable.run();
                    this.cancel();
                }
            }
        }.schedule(plugin, 0L, 50L, TimeUnit.MILLISECONDS);
    }

    public static void waitForFutures(Plugin plugin, Runnable runnable, Future<?>... futures) {
        new BungeeRunnable() {
            @Override
            public void run() {
                for(Future<?> future : futures) {
                    if(!future.isCancelled() && !future.isDone()) {
                        return;
                    }
                }
                runnable.run();
                this.cancel();
            }
        }.schedule(plugin, 0L, 50L, TimeUnit.MILLISECONDS);
    }

    public static <K> K get(Future<K> future) {
        try {
            return future.get();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
