package life.weaken.bungee.utils;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.concurrent.TimeUnit;

public abstract class BungeeRunnable implements Runnable {

    private ScheduledTask task;
    private Plugin plugin;

    public void cancel() {
        if(task == null) {
            ProxyServer.getInstance().getScheduler().schedule(plugin, this::cancel, 10, TimeUnit.MILLISECONDS);
            return;
        }
        task.cancel();
    }

    public void schedule(Plugin plugin, Long l1, Long l2, TimeUnit unit) {
        this.plugin = plugin;
        task = ProxyServer.getInstance().getScheduler().schedule(plugin, this, l1, l2, unit);
    }

}
