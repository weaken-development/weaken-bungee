package life.weaken.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class LobbyCommand extends Command {

    private final String lobbyServer;

    public LobbyCommand(String lobbyServer) {
        super("lobby", "bungee.lobby", "whub", "wlobby");
        this.lobbyServer = lobbyServer;
        this.setPermissionMessage(ChatColor.RED + "You do not have the required permission for this command");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if(!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage(new ComponentBuilder("This command may only be executed by players").color(ChatColor.RED).create());
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        commandSender.sendMessage(new ComponentBuilder("Sending you to the lobby...").color(ChatColor.GREEN).create());
        player.connect(ProxyServer.getInstance().getServerInfo(lobbyServer));
    }
}
