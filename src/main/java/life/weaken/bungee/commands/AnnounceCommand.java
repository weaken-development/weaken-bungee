package life.weaken.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.plugin.Command;

public class AnnounceCommand extends Command {

    public AnnounceCommand() {
        super("announce", "bungee.announce", "a");
        this.setPermissionMessage(ChatColor.RED + "You do not have the required permission for this command");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(args.length < 1) {
            commandSender.sendMessage(new ComponentBuilder("Incorrect usage: /announce <message>").color(ChatColor.RED).create());
            return;
        }

        String message = String.join(" ", args);
        message = ChatColor.translateAlternateColorCodes('&', message);
        message = message.replace("\\n", "\n");

        commandSender.sendMessage(new ComponentBuilder("Broadcasting message...").color(ChatColor.GREEN).create());
        ProxyServer.getInstance().broadcast(new ComponentBuilder(message).create());
    }

}
